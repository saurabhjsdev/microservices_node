# microservices_node

## Project Introduction
This project is just about topic what is microserice architecture in node js ans how it works and communicate with each other.
In this sample project we have used api gateway for communication there are others way to we can explore.


## Getting started
->Do npm i in all 3 microservices:payment,order,api-gateway
->To start microservice use command: npm start
Note:No Database we have used here as its just to understand concept if you want you can do that also. 

## Testing
 For Order Microservice:
 http://localhost:9001/order/
 http://localhost:9001/order/order-list

 For Payment Microservice:
  http://localhost:9001/payment/
 http://localhost:9001/payment/payment-list


## Author
Saurabh Mankar
